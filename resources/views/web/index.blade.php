{{-- llamdo a la cabecera  --}}
@extends('layouts.header')
{{-- se puede agregar nuevos estilos en la vista --}}
{{--<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"> --}}
@section('styles')
<style>

</style>

@endsection
{{-- contenido principal de la vista --}}
@section('content')
<div class="rev_slider_wrapper fullwidthbanner-container">
    <div id="rev-slider3" class="rev_slider fullwidthabanner">
        <ul>
            <!-- Slide -->

            <li data-transition="random">
                <!-- Main Image -->
                <img src="assets/img/slider/edit9.jpeg" data-bgposition="center center" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow font-weight-600"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['65','65','65','65']"
                    data-lineheight="['68','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;"
                    data-mask_out="x:inherit;y:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    EMPRESA CUSQUEÑA <br>PERUANA <span class="text-accent-color">DCRASUR.SAC</span><br>
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['66','76','86','96']"
                    data-fontsize="['30','30','30','30']"
                    data-lineheight="['30','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    peruanos especializados con servicios de gran calidad y excelencia<br>
                    Visitanos
                </div>
                <div class="sfb tp-caption"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['160','170','180','190']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                    <a target="_blank" href="https://www.facebook.com/Dcrasur-SAC-103375682214129" class="wprt-button outline rounded-3px">FACEBOOK </a>
                </div>
            </li>

            <li data-transition="random">
                <!-- Main Image -->
                <img src="assets/img/slider/edit6.jpeg" alt="" data-bgposition="center center" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow font-weight-600"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['65','65','65','65']"
                    data-lineheight="['68','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;"
                    data-mask_out="x:inherit;y:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    EMPRESA CUSQUEÑA <br>PERUANA <span class="text-accent-color">DCRASUR.SAC</span><br>
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['66','76','86','96']"
                    data-fontsize="['30','30','30','30']"
                    data-lineheight="['30','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    peruanos especializados con servicios de gran calidad y excelencia<br>
                    en el rubro de Fabricaciòn e Instalaciòn de Concreto Pre - Fabricado.<br><br>
                    Visitanos
                </div>
                <div class="sfb tp-caption"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['160','170','180','190']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                    <a target="_blank" href="https://www.instagram.com/dcrasur.sac/" class="wprt-button outline rounded-3px">INSTRAGRAN </a>
                </div>
            </li>
            <li data-transition="random">
                <!-- Main Image -->
                <img src="assets/img/slider/edit7.jpeg" data-bgposition="center center" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow font-weight-600"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['65','65','65','65']"
                    data-lineheight="['68','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;"
                    data-mask_out="x:inherit;y:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    EMPRESA CUSQUEÑA <br>PERUANA <span class="text-accent-color">DCRASUR.SAC</span><br>
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['66','76','86','96']"
                    data-fontsize="['30','30','30','30']"
                    data-lineheight="['30','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    "El trabajo más productivo es el que sale de las manos de un hombre contento"<br>
                    Visitanos
                </div>
                <div class="sfb tp-caption"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['160','170','180','190']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                    <a target="_blank" href="https://www.facebook.com/Dcrasur-SAC-103375682214129" class="wprt-button outline rounded-3px">FACEBOOK </a>
                </div>
            </li>
            <li data-transition="random">
                <!-- Main Image -->
                <img src="assets/img/slider/edit8.jpeg" data-bgposition="center center" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow font-weight-600"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['65','65','65','65']"
                    data-lineheight="['68','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;"
                    data-mask_out="x:inherit;y:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    EMPRESA CUSQUEÑA <br>PERUANA <span class="text-accent-color">DCRASUR.SAC</span><br>
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['66','76','86','96']"
                    data-fontsize="['30','30','30','30']"
                    data-lineheight="['30','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    "No hay ningún secreto para el éxito. Es el resultado de la preparación,<br>
                    el trabajo y aprender del fracaso"<br>
                    Visitanos
                </div>
                <div class="sfb tp-caption"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['160','170','180','190']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                    <a target="_blank" href="https://www.instagram.com/dcrasur.sac/" class="wprt-button outline rounded-3px">INSTRAGRAN </a>
                </div>
            </li>
            <!-- End Slide -->
            <!-- Slide -->
            <li data-transition="random">
                <!-- Main Image -->
                <img src="assets/img/slider/9.jpeg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow text-center font-weight-600"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['70','70','70','70']"
                    data-lineheight="['70','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                    data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on" >
                    <span class="text-accent-color">DCRASUR.SAC</span><br>
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow text-center"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['32','42','52','62']"
                    data-fontsize="['35','35','35','35']"
                    data-lineheight="['28','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                    data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    "Déjame contarte el secreto que me ha llevado a mi objetivo.<br>
                    Mi fuerza reside únicamente en mi tenacidad"<br>
                    Visitanos
                </div>
                <div class="sfb tp-caption"
                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['128','138','148','158']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                    <a href="https://www.instagram.com/dcrasur.sac/" class="wprt-button big outline rounded-3px" target="_blank">INSTRAGRAN</a>
                </div>
            </li>
            <li data-transition="random">
                <!-- Main Image -->
                <img src="assets/img/slider/edit11.jpeg" data-bgposition="center center" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow font-weight-600"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['65','65','65','65']"
                    data-lineheight="['68','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;"
                    data-mask_out="x:inherit;y:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    EMPRESA CUSQUEÑA <br>PERUANA <span class="text-accent-color">DCRASUR.SAC</span><br>
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['66','76','86','96']"
                    data-fontsize="['30','30','30','30']"
                    data-lineheight="['30','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    "Deja que el futuro diga la verdad y evalúe a cada uno según su trabajo y logros.<br>
                    El presente es de ellos; el futuro, por el que realmente he trabajado, es mío"<br>
                    Visitanos
                </div>
                <div class="sfb tp-caption"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['160','170','180','190']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                    <a target="_blank" href="https://www.facebook.com/Dcrasur-SAC-103375682214129" class="wprt-button outline rounded-3px">FACEBOOK </a>
                </div>
            </li>
            <li data-transition="random">
                <!-- Main Image -->
                <img src="assets/img/slider/edit12.jpeg" data-bgposition="center center" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow font-weight-600"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['65','65','65','65']"
                    data-lineheight="['68','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;"
                    data-mask_out="x:inherit;y:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    EMPRESA CUSQUEÑA <br>PERUANA <span class="text-accent-color">DCRASUR.SAC</span><br>
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['66','76','86','96']"
                    data-fontsize="['30','30','30','30']"
                    data-lineheight="['30','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    "Siempre que te pregunten si puedes hacer un trabajo,<br> 
                    contesta que sí y ponte enseguida a aprender como se hace"<br>
                    Visitanos
                </div>
                <div class="sfb tp-caption"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['160','170','180','190']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                    <a target="_blank" href="https://www.instagram.com/dcrasur.sac/" class="wprt-button outline rounded-3px">INSTRAGRAN </a>
                </div>
            </li>
            <li data-transition="random">
                <!-- Main Image -->
                <img src="assets/img/slider/edit14.jpeg" data-bgposition="center center" data-no-retina>
                <!-- Layers -->
                <div class="sfb tp-caption tp-resizeme text-white font-family-heading text-shadow font-weight-600"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['-52','-42','-32','-22']"
                    data-fontsize="['65','65','65','65']"
                    data-lineheight="['68','64','60','56']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_in="x:0px;y:0px;"
                    data-mask_out="x:inherit;y:inherit;"
                    data-start="1500"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on">
                    EMPRESA CUSQUEÑA <br>PERUANA <span class="text-accent-color">DCRASUR.SAC</span><br>
                </div>
                <div class="sfb tp-caption tp-resizeme text-white text-shadow"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['66','76','86','96']"
                    data-fontsize="['30','30','30','30']"
                    data-lineheight="['30','30','30','28']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-start="1800"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-lasttriggerstate="reset">
                    "Una máquina puede hacer el trabajo de 50 hombres corrientes.<br>
                    Pero no existe ninguna máquina que pueda <br>
                    hacer el trabajo de un hombre extraordinario"<br>
                    Visitanos
                </div>
                <div class="sfb tp-caption"
                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                    data-y="['middle','middle','middle','middle']" data-voffset="['160','170','180','190']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                    data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                    data-transform_idle="o:1;" 
                    data-transform_in="o:0" 
                    data-transform_out="o:0" 
                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                    data-start="2100"
                    data-splitin="none"
                    data-splitout="none"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"20px"}]'
                    data-responsive="on">
                    <a target="_blank" href="https://www.facebook.com/Dcrasur-SAC-103375682214129" class="wprt-button outline rounded-3px">FACEBOOK </a>
                </div>
            </li>
            <!-- End Slide -->
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>

<!-- Main Content -->
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    <!-- HOW WE BUILD -->
                    <section class="wprt-section how-we-build" id="quienes">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2 class="text-center margin-bottom-10">¿QUIENES SOMOS?</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="25" data-mobi="25" data-smobi="20"></div>

                                    <p class="wprt-subtitle text-center">
                                        Somos un grupo empresarial cusqueña peruana, 
                                        nos hemos especializado en la fabricacion e instalacion de concretos 
                                        Pre-Fabricados, con una gran cantidad de proyectos que han permitido 
                                        la integración del país.</p>

                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-12 -->

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-3 width-90">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-engineer"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">OBRAS</a></h3>
                                            <p>Cada obra que se realiza se hacer mediante una supervision 
                                                para tener un trabajo de calidad  y este a su agrado del cliente</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-3 width-90">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-ruler-1"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">MEDIDAS</a></h3>
                                            <p>antes de la instalacion de los cercos y postes verificamos el 
                                                suelo para la escabacion y instalacion de los prefabricados</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-3 width-90">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-garden-fence"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">SEGURIDAD</a></h3>
                                            <p>cada personal estara totalmente uniformado con su EPP de seguridad 
                                                correspondiente, eso se realiza para evitar cualquier accidente</p>
                                        </div>
                                    </div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                    <!-- WORKS -->
                    <section class="wprt-section works parallax" id="proyecto">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2 class="text-center text-white margin-bottom-10">IMAGENES DE PROYECTOS</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="36" data-mobi="36" data-smobi="36"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->

                        <div class="wprt-project arrow-style-2 has-bullets bullet-style-2" data-layout="slider" data-column="4" data-column2="3" data-column3="2" data-column4="1" data-gaph="1" data-gapv="1">
                            <div id="projects" class="cbp">
                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <div class="grid">
                                            <figure class="effect-sadie">
                                                <img src="assets/img/foto/img1.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>IZCUCHACA ANTA</a></h2>
                                                        <p>Cliente-Grimaldo</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>
                                            </div>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img1.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-honey">
                                                <img src="assets/img/foto/img2.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>INQUILPATA</a></h2>
                                                        <p>Cliente-Marcelo</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img2.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="assets/img/foto/img7.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>IZCUCHACA ANTA</a></h2>
                                                        <p>Cliente-</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img7.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-zoe">
                                                <img src="assets/img/foto/img4.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>INQUILPATA</a></h2>
                                                        <p>Cliente-Ubaldo</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img4.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-oscar">
                                                <img src="assets/img/foto/img1.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>IZCUCHACA ANTA</a></h2>
                                                        <p>Cliente-lorenza</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img1.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="assets/img/foto/img6.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>PLANTA DCRASUR</a></h2>
                                                        <p>Postes de concreto</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img6.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->

                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="assets/img/foto/img3.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>INQUILPATA</a></h2>
                                                        <p>Cliente-marcelo</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img3.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->
                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="assets/img/foto/img9.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>INQUILPATA</a></h2>
                                                        <p>Cliente-Marcelo</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img9.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->
                                <div class="cbp-item">
                                    <div class="project-item">
                                        <div class="inner">
                                            <figure class="effect-sadie">
                                                <img src="assets/img/foto/img8.jpg" alt="image" />
                                                <figcaption>
                                                    <div>
                                                        <h2><a>IZCUCHACA ANTA</a></h2>
                                                        <p>Cliente-Lorenza</p>
                                                    </div>
                                                </figcaption>           
                                            </figure>

                                            <a class="project-zoom cbp-lightbox" href="assets/img/foto/img8.jpg" data-title="LUXURY BUILDINGS">
                                                <i class="fa fa-search-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--/.cbp-item -->
                            </div><!-- /#projects -->
                        </div><!--/.wprt-project -->

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                    <!-- WHY CHOOSE US -->
                    <section class="wprt-section why-choose-us">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2 class="text-center margin-bottom-10">RECOMENDACIONES PARA MEDIDAS BASICAS PARA SEGURIDAD</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="25" data-mobi="25" data-smobi="20"></div>

                                    <p class="wprt-subtitle text-center">Toda construcción en el Perú está sometida a estrictas normas relacionadas 
                                        con la seguridad de los obreros que en ella trabajan. Una manera de reconocer una obra de construcción 
                                        legal y confiable es observando que cumple con estas normas.
                                        Entre estas medidas de seguridad, están las siguientes:</p>

                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-12 -->
                                    
                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-drawing"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">ARNESES</a></h3>
                                            <p>Para trabajos en altura, es obligatorio el uso de sogas, 
                                                arneses y otros elementos de seguridad complementarios 
                                                que garanticen la seguridad del trabajador. Los arneses 
                                                deben ser usados en todo momento. Por ejemplo, es temerario 
                                                realizar el pintado de la fachada de un edificio sin arnés.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="45" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-engineer"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">CASCOS</a></h3>
                                            <p>Durante una construcción, todo debe estar perfectamente vigilado 
                                                para que ninguna herramienta o material de construcción caiga. 
                                                Pero, previendo esta eventualidad, el uso de cascos es obligatorio 
                                                en todos los trabajadores. Incluso se exige su uso a quienes visitan la obra.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-drawing-compass"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">CHALECOS REFRACTANTES</a></h3>
                                            <p>La utilidad de estos chalecos es múltiple; sirven como protección contra 
                                                productos químicos o corrosivos utilizados en la preparación de mezclas 
                                                y pegamentos. Por otro lado, sirven para una fácil identificación y 
                                                ubicación de los trabajadores.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="45" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-drill-2"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">GUANTES GRUESOS</a></h3>
                                            <p>La manipulación de los materiales de construcción es un trabajo duro, 
                                                debido en parte a la cantidad de fierros, cemento, cal, brea y pegamentos 
                                                que requieren protección en las manos. De otra manera, producirían 
                                                quemaduras y lesiones graves en los obreros.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-light-bulb"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">BARANDAS</a></h3>
                                            <p>A veces son de madera, otras son redes fosforescentes y en otros casos
                                                se usan hasta cadenas. La finalidad de las barandas es doble: por un 
                                                lado evitar un accidente, y por otro delimitar el área que se está 
                                                trabajando pero que aún está incompleta.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="45" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box outline rounded icon-effect-2 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-garden-fence"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18"><a href="#">MALLAS PROTECTORAS</a></h3>
                                            <p>Son esas enormes telas verdes que se despliegan en los exteriores de 
                                                las construcciones. Su finalidad es proteger a las construcciones 
                                                aledañas contra el polvo y otros objetos que podrían desprenderse 
                                                por accidente.</p>
                                        </div>
                                    </div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="60" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                    <!-- SERVICES -->
                    <section class="wprt-section service" id="mision">
                        <div class="container-fluid padding-0">
                            <div class="row margin-0">
                                <div class="col-md-6 padding-0">
                                    <img src="assets/img/foto/portada1.jpg" alt="image">
                                </div><!-- /.col-md-6 -->
                                <div class="col-md-6 padding-0">
                                    <div class="wprt-content-box style-2">
                                        <h2 class="margin-bottom-10">MISION</h2>
                                        <div class="wprt-lines style-5 custom-2">
                                            <div class="line-1"></div>
                                        </div>
                                        <div class="wprt-spacer" data-desktop="25" data-mobi="25" data-smobi="25"></div>
                                        <p class="wprt-subtitle left">Satisfacer las necesidades de nuestros 
                                            clientes, con productos y servicios asociados a proyectos de Obras 
                                            de cercos de concreto Pre-Fabricados, incorporando alto grado de 
                                            compromiso y calidad en nuestro desempeño.</p>
                                        <ul class="wprt-list style-4 accent-color margin-top-30 margin-bottom-25">
                                            <li>Que nuestro cliente por nuestro trabajo</li>
                                            <li>Capacitarnos mas en el ambito del trabajo</li>
                                            <li>Trabajo en equipo</li>                                       
                                        </ul>
                                    </div><!-- /.wprt-content-box -->
                                </div><!-- /.col-md-6 -->
                            </div><!-- /.row -->
                        </div><!-- /.container-fluid -->
                    </section>                  
                    <section class="wprt-section">
                        <div class="container-fluid padding-0">
                            <div id="map" style="width:100%; height: 400px;"></div>
                        </div><!-- /.container-fluid -->
                    </section>
                    <section class="wprt-section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2>CATEGORIAS</h2>
                                    <div class="wprt-lines style-5 custom-3">
                                        <div class="line-1"></div>
                                        <div class="line-2"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>

                                    <div class="wprt-toggle style-5">
                                        <h3 class="toggle-title">Consejos</h3>
                                        <div class="toggle-content">Toda construcción en el Perú está sometida a estrictas normas relacionadas 
                                            con la seguridad de los obreros que en ella trabajan. Una manera de reconocer una obra de construcción 
                                            legal y confiable es observando que cumple con estas normas.</div>
                                    </div>

                                    <div class="wprt-toggle active style-5">
                                        <h3 class="toggle-title">Construcción</h3>
                                        <div class="toggle-content">Una vivienda se puede construir de diferentes maneras: hormigón
                                            armado y mampostería (bloques o ladrillos), adobe, madera, caña guadua, quincha o sistemas mixtos.
                                            Lo más importante de toda construcción son los materiales y las técnicas de construcción. ¡Una vivienda
                                            mal construida con cualquier material que se utilice es peligrosa! Busca
                                            ayuda profesional.</div>
                                    </div>

                                    <div class="wprt-toggle style-5">
                                        <h3 class="toggle-title">Seguridad</h3>
                                        <div class="toggle-content">Esta norma establece las condiciones de seguridad para la prevención contra incendios. 
                                            Se aplica en aquellos lugares donde las mercancías, materias primas, productos o subproductos que se manejan en los procesos,
                                            operaciones y actividades que impliquen riesgos de incendio.</div>
                                    </div>

                                    <div class="wprt-toggle style-5">
                                        <h3 class="toggle-title">Obras</h3>
                                        <div class="toggle-content">Una vivienda dañada debe obtener permisos del municipio antes
                                            de empezar a reparar o reconstruir, averigua los procedimientos a
                                            seguir en tu municipio y respeta la norma de construcción.</div>
                                    </div>
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-6">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                    <h2>LABORES</h2>
                                    <div class="wprt-lines style-5 custom-3">
                                        <div class="line-1"></div>
                                        <div class="line-2"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>

                                    <div class="wprt-galleries galleries w-570px" data-width="135" data-margin="10">
                                        <div id="wprt-slider" class="flexslider">
                                            <ul class="slides">
                                                <li class="flex-active-slide">
                                                    <a class="zoom" href="assets/img/foto/img1.jpg"><i class="fa fa-search-plus"></i></a>
                                                    <img src="assets/img/foto/img1.jpg" alt="image" />
                                                </li>

                                                <li class="flex-active-slide">
                                                    <a class="zoom" href="assets/img/foto/img2.jpg"><i class="fa fa-search-plus"></i></a>
                                                    <img src="assets/img/foto/img2.jpg" alt="image" />
                                                </li>

                                                <li class="flex-active-slide">
                                                    <a class="zoom" href="assets/img/foto/img3.jpg"><i class="fa fa-search-plus"></i></a>
                                                    <img src="assets/img/foto/img3.jpg" alt="image" />
                                                </li>

                                                <li class="flex-active-slide">
                                                    <a class="zoom" href="assets/img/foto/img4.jpg"><i class="fa fa-search-plus"></i></a>
                                                    <img src="assets/img/foto/img4.jpg" alt="image" />
                                                </li>
                                            </ul>
                                        </div>

                                        <div id="wprt-carousel" class="flexslider">
                                            <ul class="slides">
                                                <li><img src="assets/img/foto/img1.jpg" alt="image"></li>
                                                <li><img src="assets/img/foto/img2.jpg" alt="image"></li>
                                                <li><img src="assets/img/foto/img3.jpg" alt="image"></li>
                                                <li><img src="assets/img/foto/img4.jpg" alt="image"></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- /.col-md-6 -->

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>


                    <!-- TESTIMONIALS -->
                    <section class="wprt-section testimonials" id="cliente" >
                        <div class="container" >
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="70" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                                <div class="col-md-12" >
                                    <h2>CLIENTES</h2>
                                    <div class="wprt-lines style-5 custom-3">
                                        <div class="line-1"></div>
                                        <div class="line-2"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                    <div class="wprt-testimonials no-stars has-outline arrow-style-2 has-arrows arrow60 arrow-light" data-layout="slider" data-column="3" data-column2="1" data-column3="1" data-column4="1" data-gaph="10" data-gapv="10">
                                        <div id="testimonials-wrap" class="cbp" >
                                            <div class="cbp-item">
                                                <div class="customer clearfix">
                                                    <div class="inner">
                                                        <div class="image"><img src="assets/img/slider/edit23.jpeg" alt="image" /></div>
                                                        <h4 class="name">Don Paulson</h4>
                                                        <div class="position">cliente</div>
                                                        <blockquote class="whisper"> Apreciado don paulo:
                                                            ¡Gracias por visitarnos y hacer su primera compra! Estamos contentos de que haya encontrado
                                                            lo que estaba buscando. Nuestro objetivo es que siempre esté satisfecho, así que avísenos 
                                                            si su nivel de satisfacción. Esperamos volver a verle de nuevo. ¡Que tengas un gran día!
                                                            Atentamente,                                                            
                                                            Tus amigos Dcrasur...</blockquote>
                                                    </div>
                                                </div>
                                            </div><!-- /.cbp-item -->

                                            <div class="cbp-item">
                                                <div class="customer clearfix">
                                                    <div class="inner">
                                                        <div class="image"><img src="assets/img/slider/edit22.jpeg" alt="image" /></div>
                                                        <h4 class="name">andrea</h4>
                                                        <div class="position">cliente</div>
                                                        <blockquote class="whisper"> Estimada Andrea:
                                                            ¡Estamos encantado de decirte que has sido nuestra cliente durante todo un año! 
                                                            Este saludo es para agradecerte por ser parte de nuestro clientes favoritos, 
                                                            Estamos muy agradecidos por tu continuo patrocinio, porque no estaríamos aquí 
                                                            sin clientes leales como tú. Podrías haber elegido cualquier otro negocio, 
                                                            pero apreciamos que te hayas quedado con nosotros. ¡Gracias de nuevo y que 
                                                            tengas un buen día!                                                 
                                                            Sinceramente,
                                                            Tus amigos Dcrasur...</blockquote>
                                                    </div>
                                                </div>
                                            </div><!-- /.cbp-item -->

                                            <div class="cbp-item">
                                                <div class="customer clearfix">
                                                    <div class="inner">
                                                        <div class="image"><img src="assets/img/slider/edit21.jpeg" alt="image" /></div>
                                                        <h4 class="name">karen</h4>
                                                        <div class="position">cliente</div>
                                                        <blockquote class="whisper">Querida Karen,
                                                            Fue un placer conocerla, y estoy muy contento de que hayamos tenido la oportunidad 
                                                            de hablar sobre lo que está buscando para su nuevo hogar. Sé que este es un momento 
                                                            emocionante y potencialmente intenso para usted, así que déjeme decirle que está en 
                                                            buenas manos. Como mencionó que le gustaría ver algunos estilos diferentes, me aseguraré 
                                                            de que nuestro equipo le brinde una variedad de opciones. Gracias por su visita, 
                                                            y espero verle pronto.Atentamente,
                                                            Dcrasur...</blockquote>
                                                    </div>
                                                </div>
                                            </div><!-- /.cbp-item -->

                                            <div class="cbp-item">
                                                <div class="customer clearfix">
                                                    <div class="inner">
                                                        <div class="image"><img src="assets/img/slider/edit20.jpeg" alt="image" /></div>
                                                        <h4 class="name">stiven</h4>
                                                        <div class="position">cliente</div>
                                                        <blockquote class="whisper">Estimado Stiven:
                                                            Estuvimos muy contentos de ayudar a encontrar el regalo perfecto para tu esposa, y quería 
                                                            extender la mano y decirte gracias por entrar de nuevo a nuestra tienda, aunque fácilmente 
                                                            podrías haber ido al local de al lado. Espero que nos visites pronto, aunque solo tengas un 
                                                            antojo, porque sé que nuestros productos son los mejores ¡Quédate y cuídate!
                                                            Los mejores deseos para tu familia.
                                                            Dcrasur...</blockquote>
                                                    </div>
                                                </div>
                                            </div><!-- /.cbp-item -->
                                        </div><!-- /#service-wrap -->
                                    </div><!-- /.wprt-service -->
                                </div><!-- /.col-md-12 -->

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                    <!-- PARTNERS -->
                    <section id="partners" class="wprt-section partners">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="30" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-spacer" data-desktop="30" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-12 -->

                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>
                </div><!-- /.page-content -->
            </div>
        </div>
    </div>
</div>
@endsection
{{-- cualquier javascript adicional que se necesite --}}
@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbhES2PZCr_AXWFA_ysoz_QPkAwkgjjNc&callback=initMap"></script>
<script>
    $(document).ready(function() {
       //console.log('carga completa');
    $('[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                scrollTop: (target.offset().top - 72)
                }, 1000, "easeInOutExpo");
                return false;
            }
            }
        });
    });
    var map;
        function initMap() {
            let latlong={lat:-13.4734893,lng:-72.1334914};// aqui cambia latiyud y longitud de su dirección
            map = new google.maps.Map(document.getElementById('map'), {
            center: latlong,
            zoom: 15
            });
            var myLatLng = new google.maps.LatLng(latlong);
            var myMarkerOptions = {
                position: myLatLng,
                map: map
            }
            var myMarker = new google.maps.Marker(myMarkerOptions);
        }
</script>
@endsection