{{-- llamdo a la cabecera  --}}
@extends('layouts.header')
{{-- se puede agregar nuevos estilos en la vista --}}
{{-- <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"> --}}

{{-- contenido principal de la vista --}}
@section('content')
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    <section class="wprt-section">
                        <div class="container">
                            <div class="row">
                            <div class="col-md-12">
                                <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>

                                    <h2 class="text-center margin-bottom-10">VALORES</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                <div class="col-md-3">
                                    <p class='dropcap text-justify'>

                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class='dropcap text-justify'>
                                        Para WH Contratistas E.I.R.L. Los valores definen la escala de importancia de los postulados éticos y morales que aplicamos en nuestros servicios.
                                    </p>
                                    <ul class="wprt-list style-2 accent-color margin-top-30 margin-bottom-25">
                                        <li>Transparencia</li>
                                        <li>Eficiencia</li>
                                        <li>Excelencia</li>
                                        <li>Trabajo en equipo</li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <p class='dropcap text-justify'>

                                    </p>
                                </div>
                                </div>
                            </div>
                        </div><!-- /.container -->
                    </section>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- cualquier javascript adicional que se necesite --}}
@section('scripts')
@endsection