{{-- llamdo a la cabecera  --}}
@extends('layouts.header')
{{-- se puede agregar nuevos estilos en la vista --}}
{{-- contenido principal de la vista --}}
@section('content')
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    {{-- <section class="wprt-section"> --}}
                        <div class="container">
                            <div id="map" style="width:600px; height: 400px;"></div>
                        </div><!-- /.container -->
                    {{-- </section> --}}
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- cualquier javascript adicional que se necesite --}}
@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbhES2PZCr_AXWFA_ysoz_QPkAwkgjjNc&callback=initMap">
</script>
<script>
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 43.5293101, lng: -5.6773233},
        zoom: 13
        });
    } 
</script>
@endsection
