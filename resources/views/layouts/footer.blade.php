{{-- estilo del boton de whatsapp --}}
<style>
.fa-whatsapp{
position:fixed;
width:60px;
height: 60px;
line-height: 63px;
bottom: 20px;
left: 25px;
background: #25d366;
color: #fff;
border-radius: 50px;
text-align: center;
font-size: 35px;
box-shadow: 0px 1px 20px  rgba(0,0,0,0.3);
z-index: 400px;
transition: all 300ms ease;
}
.btn-whatsapp:hover{
background:#55e7bb;
}

</style>
{{-- final del estilo de whatsapp --}}

<!-- Footer -->
<section id="contacto">
    <footer id="footer">
        <div id="footer-widgets" class="container style-5">
            <div class="row">
                <div class="col-md-4">
                    <div class="widget widget_text" >
                        <h2 class="widget-title" ><span>SOBRE NOSOTROS</span></h2>
                        <div class="textwidget">
                            <img src="{{asset('assets/img/logo10.png')}}"  width="200px" height="80px"/>
                            <p>Somos una empresa peruana especializada en servicios de gran calidad y excelencia en la Fabricación e Instalación de 
                            Concreto Pre - fabricados</p>
                        </div>
                    </div>
                </div>
    
                {{-- <div class="col-md-4">
                    <div class="widget widget_links">
                        <h2 class="widget-title"><span>LINKS DE COMPANIAS</span></h2>
                        <ul class="wprt-links clearfix col2">
                            <li class="style-2"><a href="#">History</a></li>
                            <li class="style-2"><a href="#">Portfolio</a></li>
                            <li class="style-2"><a href="#">Contact Us</a></li>
                            <li class="style-2"><a href="#">Shop</a></li>
                        </ul>
                    </div>
                </div> --}}
    
                <div class="col-md-4">
                    <div class="widget widget_information">
                        <h2 class="widget-title"><span>INFORMACIÓN DE CONTACTO</span></h2>
                        <ul class="style-2">
                            <li class="address clearfix">
                                <span class="hl">Dirección:</span>
                                <span class="text">CALLE.PICHOC VALLE DE CACHIMAYO S/N ANTA/ANTA-CUSCO</span>
                            </li>
                            <li class="phone clearfix">
                                <span class="hl">Celular:</span> 
                                <span class="text">+51 965-916-493</span>
                            </li>
                            <li class="email clearfix">
                                <span class="hl">E-mail: *</span>
                                <span class="text"><a target="_blank" href="https://mail.google.com/mail/u/0/">dcrasur@gmail.com</a></span>
                                <span class="hl">*</span>
                                <span class="text"><a target="_blank" href="https://mail.google.com/mail/u/0/">rony@dcrasur.com</a></span>
                                <span class="hl">*</span >
                                <span class="text"><a target="_blank" href="https://mail.google.com/mail/u/0/">milagros@dcrasur.com</a></span>
                            </li>
                        </ul>
                    </div>
    
                    <div class="widget widget_spacer">
                        <div class="wprt-spacer clearfix" data-desktop="10" data-mobi="10" data-smobi="10"></div>
                    </div>
    
                    <div class="widget widget_socials">
                        <div class="socials">
                            {{-- <a target="_blank" href="#"><i class="fa fa-whatsapp"></i></a> --}}
                            <a target="_blank" href="https://www.facebook.com/Dcrasur-SAC-103375682214129"><i class="fa fa-facebook"></i></a>
                            <a target="_blank" href="https://www.instagram.com/dcrasur.sac/"><i class="fa fa-instagram"></i></a>
                            {{-- <a target="_blank" href="#"><i class="fa fa-pinterest"></i></a> --}}
                            {{-- <a target="_blank" href="#"><i class="fa fa-dribbble"></i></a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        {{-- ruta para whatsapp--}}
        <a class="fa fa-whatsapp" href="https://wa.me/+51965916493/?text=Hola%20buenas,%20hemos%20visitado%20su%20pagina%20web%20y%20sus%20redes%20sociales%20estoy%20interesado%20en%20su%20producto%20podria%20ayudarnos%20estare%20en%20la%20espera%20gracias.." target="_blank"></a>
    </footer>
</section>


</div><!-- /#page -->
</div><!-- /#wrapper -->

<a id="scroll-top"></a>


<!-- Javascript -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/animsition.js') }}"></script>
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/countTo.js') }}"></script>
<script src="{{ asset('assets/js/flexslider.js') }}"></script>
<script src="{{ asset('assets/js/owlCarousel.js') }}"></script>
<script src="{{ asset('assets/js/cube.portfolio.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>

<!-- Revolution Slider -->
<script src="{{ asset('assets/slider/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('assets/js/rev-slider.js') }}"></script>

<!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->  
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.video.min.js') }}"></script>
@yield('script')
</body>
</html>