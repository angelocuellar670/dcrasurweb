<!DOCTYPE html>
<html class="no-js" lang="{{app()->getLocale()}}">

<head>
    <meta charset="utf-8">
    <title>DCRASUR</title>
    <meta name="description" content="construcción, servicios de construcción, arquitectura, ingeniería, servicio de enchapado y otros servicios relacionados con la construcción">
    <meta name="keywords" content="servicios de construcción, arquitectura, ingenieria ">
    <meta http-equiv="owner"      content="Fcp - av. Horacio urtega 1477- Jesus Maria - Lima - Perú">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="{{ asset('style.css') }}" rel="stylesheet" type="text/css">
    {{-- <link href="{{ asset('resource/css/app.css') }}" rel="stylesheet" type="text/css"> --}}
    {{-- <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}"> 
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}"> --}}
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/icon/favicon.png') }}">
    <link rel="apple-touch-icon-precomposed"  href="{{ asset('icon/apple-touch-icon-158-precomposed.png')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="front-page no-sidebar site-layout-full-width header-style-5 menu-has-cart header-sticky">
    <div id="wrapper" class="animsition">
    <div id="page" class="clearfix">
        <div id="site-header-wrap">
            <!-- Header -->
            <header id="site-header" class="header-front-page style-5">
                <div id="site-header-inner" class="container">
                    <div class="wrap-inner">          
                        <div id="site-logo" class="clearfix">
                            <div id="site-logo-inner">
                                <a href="{{asset('/')}}" title="Construction" rel="home" class="main-logo">
                                    {{-- <img src="assets/img/logo-light.png" alt="Construction" data-retina="assets/img/logo-light@2x.png" data-width="200" data-height="30"> --}}
                                    <img src="{{asset('assets/img/dcrasurlogo.png')}}" alt="Construction" data-retina="{{ asset('assets/img/dcrasurlogo.png')}}"  data-width="200" data-height="30">
                                </a>
                            </div>
                        </div><!-- /#site-logo -->
        
                        <div class="mobile-button"><span></span></div><!-- //mobile menu button -->
        
                        <nav id="main-nav" class="main-nav">
                            <ul class="menu">
                                <li class="menu-item"><a href="#quienes">Nosotros</a>
                                    {{-- <li class="menu-item"><a href="{{asset('web/nosotros')}}">Nosotros</a> --}}
                                    <ul class="sub-menu">
                                    {{--  <li class="menu-item"><a href="{{asset('web/misionvision')}}">Misión, Visión</a></li>
                                        <li class="menu-item"><a href="{{asset('web/valores')}}">Valores</a></li>
                                        <li class="menu-item"><a href="{{asset('web/cultura')}}">Cultura</a></li>
                                        <li class="menu-item"><a href="{{asset('web/politicas')}}">Políticas</a></li> --}}
                                        {{-- <li class="menu-item"><a href="home-hero-slideshow.html">Valores</a></li>
                                        <li class="menu-item"><a href="home-hero-slideshow-2.html">Politicas</a></li> --}}
                                        {{-- <li class="menu-item"><a href="home-header-fixed.html">Home 1 - Header Fixed</a></li>
                                        <li class="menu-item"><a href="home-2-slider-simple.html">Home 2 - Simple Slider</a></li>
                                        <li class="menu-item"><a href="home-3-hero-slideshow.html">Home 3 - Slideshow</a></li> --}}
                                    </ul>
                                </li>

                                <li class="menu-item"><a href="#proyecto">PROYECTOS</a>
                                    <ul class="sub-menu">
                                        {{-- <li class="menu-item"><a href="page-contact.html">Contact</a></li>
                                        <li class="menu-item"><a href="page-about.html">About Us</a></li>
                                        <li class="menu-item"><a href="page-about-2.html">About Us 2</a></li>
                                        <li class="menu-item"><a href="page-services.html">Services</a></li>
                                        <li class="menu-item"><a href="page-services-2.html">Services 2</a></li>
                                        <li class="menu-item"><a href="page-service-detail.html">Service Detail</a></li>
                                        <li class="menu-item"><a href="page-shop.html">Shop</a></li>
                                        <li class="menu-item"><a href="page-shop-single.html">Shop Single</a></li>
                                        <li class="menu-item"><a href="page-shop-cart.html">Shop Cart</a></li>
                                        <li class="menu-item"><a href="blog-single.html">Blog Single</a></li> --}}
                                    </ul>
                                </li>
                                <li class="menu-item"><a href="#mision">MISION Y VISION</a>
                                    <ul class="sub-menu">
                                        {{-- <li class="menu-item"><a href="icons.html">Construction Icons</a></li>
                                        <li class="menu-item"><a href="buttons.html">Buttons</a></li>
                                        <li class="menu-item"><a href="toggles.html">Toggles</a></li>
                                        <li class="menu-item"><a href="typography.html">Typography</a></li>
                                        <li class="menu-item"><a href="galleries.html">Galleries</a></li>
                                        <li class="menu-item"><a href="iconboxs.html">IconBoxs</a></li> --}}
                                    </ul>
                                </li>
                                <li class="menu-item"><a href="#cliente">CLIENTES</a>
                                {{--  <ul class="sub-menu">
                                        <li class="menu-item"><a href="page-portfolio-grid.html">Grid</a></li>
                                        <li class="menu-item"><a href="page-portfolio-grid-fullwidth.html">Grid Full-Width</a></li>
                                        <li class="menu-item"><a href="page-portfolio-slider.html">Slider</a></li>
                                        <li class="menu-item"><a href="page-portfolio-slider-fullwidth.html">Slider Full-Width</a></li>
                                        <li class="menu-item"><a href="page-project-detail.html">Project Detail 1</a></li>
                                        <li class="menu-item"><a href="page-project-detail-2.html">Project Detail 2</a></li>
                                        <li class="menu-item"><a href="page-project-detail-3.html">Project Detail 3</a></li>
                                        <li class="menu-item"><a href="page-project-detail-4.html">Project Detail 4</a></li>                               
                                    </ul> --}}
                                </li>
                                {{-- <li class="menu-item"><a href="{{asset('admin/login')}}">login</a></li> --}}
                                {{-- <li class="menu-item"><a href="#">login</a></li> --}}
                                <li class="menu-item"><a href="#contacto">Contacto</a></li>
                            </ul>
                        </nav><!-- /#main-nav -->
        
                        {{-- <div class="nav-top-cart-wrapper">
                            <a class="nav-cart-trigger" href="#">
                                <span class="fa fa-shopping-cart cart-icon">
                                    <span class="shopping-cart-items-count">3</span>
                                </span>
                            </a>
        
                            <div class="nav-shop-cart">
                                <div class="widget_shopping_cart_content">
                                    <ul class="cart_list product_list_widget ">
                                        <li class="mini_cart_item">
                                            <a href="#" class="remove" title="Remove this item">×</a>
                                            <a href="#">
                                                <img width="160" height="160" src="assets/img/shop-item.png" alt="image">
                                                Shop Item 01
                                            </a>
                                                        
                                            <span class="quantity">1 × <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">$</span>200.00</span>
                                            </span>
                                        </li>
                                        <li class="mini_cart_item">
                                            <a href="#" class="remove" title="Remove this item">×</a>
                                            <a href="#">
                                                <img width="160" height="160" src="assets/img/shop-item.png" alt="image">
                                                Shop Item 02
                                            </a>
                                                        
                                            <span class="quantity">1 × <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">$</span>70.00</span>
                                            </span>
                                        </li>
                                        <li class="mini_cart_item">
                                            <a href="#" class="remove" title="Remove this item">×</a>
                                            <a href="#">
                                                <img width="160" height="160" src="assets/img/shop-item.png" alt="image">
                                                Shop Item 03
                                            </a>
                                                        
                                            <span class="quantity">1 × <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">$</span>30.00</span>
                                            </span>
                                        </li>
                                    </ul><!-- /.product_list_widget -->
        
                                    <p class="total">
                                        <strong>Subtotal:</strong>
                                        <span class="woocommerce-Price-amount amount">
                                            <span class="woocommerce-Price-currencySymbol">$</span>300.00
                                        </span>
                                    </p>
                
                                    <p class="buttons">
                                        <a href="#" class="wprt-button small">View Cart</a>
                                        <a href="#" class="wprt-button small checkout">Checkout</a>
                                    </p>
                                </div>
                            </div>
                        </div><!-- /.nav-top-cart-wrapper --> --}}
                    </div>
                </div><!-- /#site-header-inner -->
            </header><!-- /#site-header -->
        </div><!-- /#site-header-wrap --> 
    
    

@yield('content')
{{-- llamado al pie de la pagina --}}

    

@include('layouts.footer')
