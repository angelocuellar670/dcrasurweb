<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', [HomeController::class,'index'])->name('inicio');
Route::group(['prefix' => 'web'], function() {
});
Route::group(['prefix'=>'web'], function(){
    Route::get('contacto','WebController@contacto')->name('web.contacto');
    Route::get('nosotros','WebController@nosotros')->name('web.nosotros');
    Route::get('misionvision','WebController@misionvision')->name('web.misionvision');
    Route::get('valores','WebController@valores')->name('web.valores');
    Route::get('cultura','WebController@cultura')->name('web.cultura');
    Route::get('politicas','WebController@politicas')->name('web.politicas');
    Route::get('servicios','WebController@servicios')->name('web.servicios');
});